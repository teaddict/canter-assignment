package controllers.formats

import models.{CanterProduct, CanterProductDetail}
import play.api.libs.json.Json

object CanterProductFormats {
  implicit val productDetailFormat = Json.format[CanterProductDetail]
  implicit val productFormat = Json.format[CanterProduct]
}
