package controllers

import com.google.inject.Inject
import controllers.formats.CanterProductFormats._
import models.CanterProduct
import play.api.Logging
import play.api.i18n.I18nSupport
import play.api.libs.json._
import play.api.mvc._
import services.CanterProductService

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.control.NonFatal

class CanterProductController @Inject()(cc: ControllerComponents,
                                        canterProductService: CanterProductService
                                       ) extends AbstractController(cc) with Logging with I18nSupport {

  def getAll: Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    logger.info("Get all products")
    canterProductService.get() map { items =>
      Ok(Json.toJson(items))
    } recover {
      case NonFatal(e) =>
        logger.error("Products could not be retrieved", e)
        InternalServerError("Products could not be retrieved")
    }
  }

  def get(id: Long): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    logger.info(s"Get product with id: ${id}")
    canterProductService.get(id) map {
      case Some(p) => Ok(Json.toJson(p))
      case None => NotFound("Product does not exists")
    } recover {
      case NonFatal(e) =>
        logger.error("Product could not be retrieved", e)
        InternalServerError("Product could not be retrieved")
    }
  }

  def delete(id: Long): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    logger.info(s"Delete product with id: ${id}")
    canterProductService.delete(id) map { _ =>
      Ok("Product is deleted")
    } recover {
      case NonFatal(e) =>
        logger.error("Product could not be deleted", e)
        InternalServerError("Product could not be deleted")
    }
  }

  def create(): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    logger.info("Create new product")
    CanterProduct.form.bindFromRequest.fold(
      errorForm => {
        logger.warn(s"Create form request json parsing error: ${errorForm.errors}")
        Future.successful(BadRequest(errorForm.errorsAsJson))
      },
      data => {
        canterProductService.create(data.copy(id=None)).map( _ => Ok("Product is created")) recover {
          case NonFatal(e) =>
            logger.error("Product could not be created", e)
            InternalServerError("Product could not be created")
        }
      })
  }

  def update(id: Long): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    logger.info(s"Update product with id: ${id}")
    CanterProduct.form.bindFromRequest.fold(
      errorForm => {
        logger.warn(s"Update form request json parsing error: ${errorForm.errors}")
        Future.successful(BadRequest(errorForm.errorsAsJson))
      },
      data => {
        canterProductService.update(data).map( _ => Ok("Product is updated")) recover {
          case NonFatal(e) =>
            logger.error("Product could not be updated", e)
            InternalServerError("Product could not be updated")
        }
      })
  }
}
