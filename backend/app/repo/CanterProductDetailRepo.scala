package repo

import com.google.inject.Singleton
import javax.inject.Inject
import models.CanterProductDetailDto
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

trait CanterProductDetailTable extends CanterProductTable with HasDatabaseConfigProvider[JdbcProfile] {
  import profile.api._

  class CanterProductDetailTable(tag: Tag) extends Table[CanterProductDetailDto](tag, "productDetails") {
    def id = column[Long]("id", O.PrimaryKey,O.AutoInc)
    def productId = column[Long]("productId")
    def key = column[String]("key")
    def value = column[String]("value")

    override def * = (id.?, productId, key, value) <> (CanterProductDetailDto.tupled, CanterProductDetailDto.unapply)

    def productFk = foreignKey("ProductId_FK", productId, canterProductTableQuery)(_.id, onDelete = ForeignKeyAction.Cascade)
  }

  val canterProductDetailQuery = TableQuery[CanterProductDetailTable]
  val canterProductDetailSchema = canterProductDetailQuery.schema
}

@Singleton
class CanterProductDetailRepo @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
                                       (implicit executionContext: ExecutionContext)
  extends CanterProductDetailTable {

  import profile.api._

  def all(): Future[Seq[CanterProductDetailDto]] = {
    db.run(canterProductDetailQuery.result)
  }

  def insert(productDetailItem: CanterProductDetailDto): Future[Long] = {
    db.run(canterProductDetailQuery returning canterProductDetailQuery.map(_.id) += productDetailItem)
  }


  def insert(productDetailItems: Seq[CanterProductDetailDto]): Future[Option[Int]] = {
    db.run(canterProductDetailQuery ++= productDetailItems)
  }

  def deleteByProductId(productId: Long): Future[Int] = {
    db.run(canterProductDetailQuery.filter(_.productId === productId).delete)
  }

  def findByProductIdQuery(productId: Long) = {
    canterProductDetailQuery.filter(_.productId === productId).result
  }

  def get(id: Long): Future[Option[CanterProductDetailDto]] = {
    db.run(canterProductDetailQuery.filter(_.id === id).result.headOption)
  }
}