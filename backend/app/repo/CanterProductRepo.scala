package repo

import com.google.inject.{Inject, Singleton}
import models.{CanterProductDetailDto, CanterProductDto}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

trait CanterProductTable extends HasDatabaseConfigProvider[JdbcProfile] {
  import profile.api._

  class CanterProductTable(tag: Tag) extends Table[CanterProductDto](tag, "products") {
    def id = column[Long]("id", O.PrimaryKey,O.AutoInc)
    def name = column[String]("name")
    def category = column[String]("category")
    def code = column[String]("code")
    def price = column[BigDecimal]("price")

    override def * = (id.?, name, category, code, price) <> (CanterProductDto.tupled, CanterProductDto.unapply)
  }

  val canterProductTableQuery = TableQuery[CanterProductTable]
  val canterProductTableSchema = canterProductTableQuery.schema
}

@Singleton
class CanterProductRepo @Inject()(protected val dbConfigProvider: DatabaseConfigProvider,
                                  val canterProductDetailRepo: CanterProductDetailRepo)
                                 (implicit executionContext: ExecutionContext)
  extends CanterProductTable {
  import profile.api._

  def insert(item: CanterProductDto): Future[Long] = {
    db.run(canterProductTableQuery returning canterProductTableQuery.map(_.id) += item)
  }

  def delete(id: Long): Future[Int] = {
    db.run(canterProductTableQuery.filter(_.id === id).delete)
  }

  def update(item: CanterProductDto): Future[Int] = {
    db.run(canterProductTableQuery.filter(_.id === item.id).update(item))
  }

  def get(id: Long): Future[(Option[CanterProductDto], Seq[CanterProductDetailDto])] = {
    val query = for {
      p <- canterProductTableQuery.filter(_.id === id).result.headOption
      pd <- canterProductDetailRepo.findByProductIdQuery(id)
    } yield (p, pd)
    db.run(query)
  }
  //this way I wanted to make only one db request to get my data
  def get(): Future[Seq[(CanterProductDto, Option[CanterProductDetailDto])]] = {
    val joinLeft = canterProductTableQuery.joinLeft(canterProductDetailRepo.canterProductDetailQuery).on(_.id === _.productId).result
    db.run(joinLeft)
  }

}