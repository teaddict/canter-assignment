package services

import com.google.inject.{ImplementedBy, Inject, Singleton}
import models.{CanterProduct, CanterProductDetail}
import repo.{CanterProductDetailRepo, CanterProductRepo}

import scala.concurrent.{ExecutionContext, Future}

@ImplementedBy(classOf[CanterProductServiceImpl])
trait CanterProductService {
  def create(canterProduct: CanterProduct): Future[Long]
  def update(canterProduct: CanterProduct): Future[Int]
  def delete(id: Long): Future[Int]
  def get(): Future[Seq[CanterProduct]]
  def get(id: Long):Future[Option[CanterProduct]]
}

@Singleton
class CanterProductServiceImpl @Inject()(canterProductRepo: CanterProductRepo,
                                         canterProductDetailRepo: CanterProductDetailRepo)(implicit executionContext: ExecutionContext)
  extends CanterProductService {

  override def create(canterProduct: CanterProduct): Future[Long] = {
    val productDto = canterProduct.createCanterProductDto
    for {
      pid <- canterProductRepo.insert(productDto)
      productDetails = canterProduct.createCanterProductDetailDto(pid)
      _ <- canterProductDetailRepo.insert(productDetails)
    } yield pid
  }

  override def update(canterProduct: CanterProduct): Future[Int] = {
    val productDto = canterProduct.createCanterProductDto
    for {
      updated <- canterProductRepo.update(productDto)
      _ <- canterProductDetailRepo.deleteByProductId(canterProduct.id.get)
      productDetails = canterProduct.createCanterProductDetailDto(canterProduct.id.get)
      _ <- canterProductDetailRepo.insert(productDetails)
    } yield updated
  }

  override def delete(id: Long): Future[Int] = canterProductRepo.delete(id)

  override def get(): Future[Seq[CanterProduct]] = {
    canterProductRepo.get().map { data =>
      data.groupBy(_._1).map {
        case(product, productAndDetails) =>
          val productDetails = productAndDetails.flatMap(_._2).map(p => CanterProductDetail(p.key, p.value))
          CanterProduct(product.id, product.name, product.category, product.code, product.price, productDetails)
      }.toSeq
    }
  }

  override def get(id: Long): Future[Option[CanterProduct]] = {
    for {
      (p,pd) <- canterProductRepo.get(id)
    } yield {
      p map { product =>
        val productDetails = pd.map(p => CanterProductDetail(p.key, p.value))
        CanterProduct(product.id, product.name, product.category, product.code, product.price, productDetails)
      }
    }
  }
}
