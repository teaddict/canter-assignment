package models

case class CanterProductDetail(key: String, value: String)
case class CanterProductDetailDto(id: Option[Long], productId: Long, key: String, value: String)