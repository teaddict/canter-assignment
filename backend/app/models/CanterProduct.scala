package models

import play.api.data.Form
import play.api.data.Forms._

case class CanterProduct(id: Option[Long],
                         name: String,
                         category: String,
                         code: String,
                         price: BigDecimal,
                         details: Seq[CanterProductDetail]) {
  def createCanterProductDto: CanterProductDto = CanterProductDto(this.id, this.name, this.category, this.code, this.price)
  def createCanterProductDetailDto(productId: Long): Seq[CanterProductDetailDto] = {
    this.details.map(d => CanterProductDetailDto(None, productId, d.key, d.value))
  }
}

object CanterProduct {
  val form: Form[CanterProduct] = Form(
    mapping(
      "id" -> optional(longNumber),
      "name" -> nonEmptyText,
      "category"-> nonEmptyText,
      "code"-> nonEmptyText,
      "price" -> bigDecimal,
      "details" -> seq(
        mapping(
          "key" -> nonEmptyText,
          "value" -> nonEmptyText
        )(CanterProductDetail.apply)(CanterProductDetail.unapply)
      ))(CanterProduct.apply)(CanterProduct.unapply)
    )
}

case class CanterProductDto(id: Option[Long],
                            name: String,
                            category: String,
                            code: String,
                            price: BigDecimal)