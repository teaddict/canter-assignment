package repo

import common.{CommonTestActions, TestDb, TestModels}
import org.scalatest.Matchers

class CanterProductRepoTest extends TestDb with CommonTestActions with Matchers {
  private val canterProductRepo = app.injector.instanceOf[CanterProductRepo]
  private val canterProductDetailRepo = app.injector.instanceOf[CanterProductDetailRepo]

  describe("CanterProductRepo") {
    it("should insert product successfully and return the id of product") {
      val id = canterProductRepo.insert(TestModels.canterProductDto).awaitForResult
      val expected = TestModels.canterProductDto.copy(Some(id))

      val result = canterProductRepo.get(id).awaitForResult._1
      result.get shouldBe expected
    }
    it("should update product successfully") {
      val id = canterProductRepo.insert(TestModels.canterProductDto).awaitForResult
      val updated = TestModels.canterProductDto.copy(Some(id), "test updated")
      canterProductRepo.update(updated).awaitForResult shouldBe 1

      val result = canterProductRepo.get(id).awaitForResult._1
      result.get shouldBe updated
    }
    it("should delete product successfully") {
      val id = canterProductRepo.insert(TestModels.canterProductDto).awaitForResult
      val result = canterProductRepo.delete(id).awaitForResult
      result shouldBe 1
    }
    it("should get product with product details") {
      val pId = canterProductRepo.insert(TestModels.canterProductDto).awaitForResult
      val pd = TestModels.canterProductDetailDto.copy(productId = pId)
      val pdId = canterProductDetailRepo.insert(pd).awaitForResult
      val expected = Seq((TestModels.canterProductDto.copy(Some(pId)), Some(pd.copy(Some(pdId)))))
      val result = canterProductRepo.get().awaitForResult
      result shouldBe expected
    }
    it("should get all products successfully") {
      canterProductRepo.insert(TestModels.canterProductDto).awaitForResult
      canterProductRepo.insert(TestModels.canterProductDto).awaitForResult
      val result = canterProductRepo.get().awaitForResult
      result.size shouldBe 2
    }
    it("should get all products with product details") {
      val pId1 = canterProductRepo.insert(TestModels.canterProductDto).awaitForResult
      val pId2 = canterProductRepo.insert(TestModels.canterProductDto).awaitForResult
      val pd1 = TestModels.canterProductDetailDto.copy(productId = pId1)
      val pd2 = TestModels.canterProductDetailDto.copy(productId = pId2)

      val pdId1 = canterProductDetailRepo.insert(pd1).awaitForResult
      val pdId2 = canterProductDetailRepo.insert(pd2).awaitForResult

      val expected1 = Seq((TestModels.canterProductDto.copy(Some(pId1)), Some(pd1.copy(Some(pdId1)))))
      val expected2 = Seq((TestModels.canterProductDto.copy(Some(pId2)), Some(pd2.copy(Some(pdId2)))))

      val result = canterProductRepo.get().awaitForResult
      result shouldBe expected1 ++ expected2
    }
  }

}
