package repo

import common.{CommonTestActions, TestDb, TestModels}
import org.scalatest.Matchers

class CanterProductDetailRepoTest extends TestDb with CommonTestActions with Matchers {
  private val canterProductDetailRepo = app.injector.instanceOf[CanterProductDetailRepo]

  describe("CanterProductDetailRepo") {
    describe("foreign key constraints disabled") {
      //disable foreign key constraints so we can run tests without creating CanterProduct
      disableRefInt
      it("should insert product detail and return the id") {
        val result = canterProductDetailRepo.insert(TestModels.canterProductDetailDto).awaitForResult
        val expected = TestModels.canterProductDetailDto.copy(Some(result))

        canterProductDetailRepo.get(result).awaitForResult.head shouldBe expected
      }

      it("should insert many product detail and return the id") {
        val productDetails = Seq(TestModels.canterProductDetailDto, TestModels.canterProductDetailDto)
        canterProductDetailRepo.insert(productDetails).awaitForResult

        canterProductDetailRepo.all().awaitForResult.size shouldBe productDetails.size
      }

      it("should get all product details") {
        val id = canterProductDetailRepo.insert(TestModels.canterProductDetailDto).awaitForResult
        val result = canterProductDetailRepo.all().awaitForResult
        val expected = TestModels.canterProductDetailDto.copy(Some(id))

        result shouldBe Seq(expected)
      }

      it("should delete product detail and return effected row count") {
        canterProductDetailRepo.insert(TestModels.canterProductDetailDto).awaitForResult
        val result = canterProductDetailRepo.deleteByProductId(TestModels.canterProductDetailDto.productId).awaitForResult

        result shouldBe 1
      }
    }
  }
}
