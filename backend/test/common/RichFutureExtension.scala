package common

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

trait RichFutureExtension {
  implicit class RichFuture[T](future: Future[T]) {
    def awaitForResult: T = Await.result(future, Duration.Inf)
  }
}
