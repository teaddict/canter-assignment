package common

import models.{CanterProduct, CanterProductDetail, CanterProductDetailDto, CanterProductDto}

object TestModels {
  val canterProductDto: CanterProductDto = CanterProductDto(None,"test product", "test category", "111", 10.99)
  val canterProductDetailDto: CanterProductDetailDto = CanterProductDetailDto(None, 1, "test key", "test value")
  val canterProductDetail: CanterProductDetail = CanterProductDetail(canterProductDetailDto.key, canterProductDetailDto.value)
  val canterProduct: CanterProduct = CanterProduct(None, canterProductDto.name, canterProductDto.category,
    canterProductDto.code, canterProductDto.price, Seq(canterProductDetail))
}
