package common

import org.scalatest.{BeforeAndAfterEach, Suite}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import repo.{CanterProductDetailTable, CanterProductTable}
import slick.jdbc.SetParameter.SetUnit
import slick.jdbc.{JdbcProfile, SQLActionBuilder}

import scala.util.Try

trait CommonTestActions
  extends HasDatabaseConfigProvider[JdbcProfile] with CanterProductTable with CanterProductDetailTable with RichFutureExtension
    with BeforeAndAfterEach {
  this: Suite with TestDb =>
  import slick.jdbc.H2Profile.api._

  override lazy val dbConfigProvider: DatabaseConfigProvider =
    app.injector.instanceOf[DatabaseConfigProvider]

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    clearDatabase()
  }

  override protected def afterEach(): Unit = {
    clearDatabase()
    super.afterEach()
  }

  def clearDatabase(): Unit = {
    Try(dropTables())
    createTables()
  }

  private def createTables(): Unit = {
    canterProductTableSchema.createStatements.toList.foreach { query =>
      db.run(SQLActionBuilder(List(query), SetUnit).asUpdate).awaitForResult
    }
    canterProductDetailSchema.createStatements.toList.foreach { query =>
      db.run(SQLActionBuilder(List(query), SetUnit).asUpdate).awaitForResult
    }
  }

  private def dropTables(): Unit = {
    canterProductTableSchema.dropStatements.toList.reverse.foreach { query =>
      db.run(SQLActionBuilder(List(query), SetUnit).asUpdate).awaitForResult
    }
    canterProductDetailSchema.dropStatements.toList.reverse.foreach { query =>
      db.run(SQLActionBuilder(List(query), SetUnit).asUpdate).awaitForResult
    }
  }

  def disableRefInt = {
    db.run(sqlu"SET foreign_key_checks = 0 ").awaitForResult
  }

}