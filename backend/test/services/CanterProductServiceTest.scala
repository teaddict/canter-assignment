package services

import common.{RichFutureExtension, TestModels}
import org.mockito.Mockito._
import org.scalatest.{FunSpec, _}
import org.scalatestplus.mockito.MockitoSugar
import repo.{CanterProductDetailRepo, CanterProductRepo}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class CanterProductServiceTest extends FunSpec with MockitoSugar with RichFutureExtension with Matchers {
  private val canterProductRepo = mock[CanterProductRepo]
  private val canterProductDetailRepo = mock[CanterProductDetailRepo]
  private val canterProductService = new CanterProductServiceImpl(canterProductRepo, canterProductDetailRepo)

  describe("CanterProductServiceTest") {
    val pid = 1234L
    it("should create a product successfully") {
      val testProductDto = TestModels.canterProductDto
      val testProductDetailDto = Seq(TestModels.canterProductDetailDto.copy(productId = pid))
      when(canterProductRepo.insert(testProductDto)).thenReturn(Future(pid))
      when(canterProductDetailRepo.insert(testProductDetailDto)).thenReturn(Future(Some(1)))

      canterProductService.create(TestModels.canterProduct).awaitForResult shouldBe pid
    }
    it("should update a product successfully") {
      val testProductDto = TestModels.canterProductDto.copy(id = Some(pid))
      val testProductDetailDto = Seq(TestModels.canterProductDetailDto.copy(productId = pid))
      when(canterProductRepo.update(testProductDto)).thenReturn(Future(1))
      when(canterProductDetailRepo.deleteByProductId(pid)).thenReturn(Future(1))
      when(canterProductDetailRepo.insert(testProductDetailDto)).thenReturn(Future(Some(1)))

      canterProductService.update(TestModels.canterProduct.copy(id = Some(pid))).awaitForResult shouldBe 1
    }
    it("should delete a product successfully") {
      when(canterProductRepo.delete(pid)).thenReturn(Future(1))

      canterProductService.delete(pid).awaitForResult shouldBe 1
    }
    it("should get single product successfully") {
      val testProductDto = TestModels.canterProductDto.copy(id = Some(pid))
      val testProductDetailDto = Seq(TestModels.canterProductDetailDto.copy(productId = pid))
      when(canterProductRepo.get(pid)).thenReturn(Future((Some(testProductDto), testProductDetailDto)))

      val expected = TestModels.canterProduct.copy(id = Some(pid))
      canterProductService.get(pid).awaitForResult shouldBe Some(expected)
    }
    it("should get all products successfully") {
      val testProductDto = TestModels.canterProductDto.copy(id = Some(pid))
      val testProductDetailDto = TestModels.canterProductDetailDto.copy(productId = pid)
      val result = Future(Seq((testProductDto, Some(testProductDetailDto))))
      when(canterProductRepo.get()).thenReturn(result)

      val expected = TestModels.canterProduct.copy(id = Some(pid))
      canterProductService.get(pid).awaitForResult shouldBe Some(expected)
    }
  }
}
