package controller

import common.{RichFutureExtension, TestModels}
import controllers.CanterProductController
import controllers.formats.CanterProductFormats._
import models.CanterProduct
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import play.api.libs.json.Json
import play.api.mvc.Result
import play.api.test.Helpers._
import play.api.test.{FakeHeaders, FakeRequest, Helpers, Injecting}
import services.CanterProductService

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
class CanterProductControllerTest extends PlaySpec with GuiceOneAppPerTest with Injecting with MockitoSugar with RichFutureExtension {
  private val canterProductService = mock[CanterProductService]
  private val controller = new CanterProductController(Helpers.stubControllerComponents(), canterProductService)

  "CanterProductController" should {
    val pid = 1234L
    val postHeaders = FakeHeaders(Seq("Content-type" -> "application/json"))
    "should get all products" in {
      val testProduct = TestModels.canterProduct.copy(id = Some(pid))
      val products = Future(Seq(testProduct))
      when(canterProductService.get()).thenReturn(products)

      val result: Future[Result] = controller.getAll.apply(FakeRequest(GET, "/products"))
      val resultAsObj = Json.fromJson[Seq[CanterProduct]](Json.parse(contentAsString(result))).get
      resultAsObj mustBe Seq(testProduct)
      status(result) mustBe OK
      contentType(result) mustBe Some("application/json")
    }
    "should get all products request fail and return internal server error if service throws exception" in {
      val testProduct = TestModels.canterProduct.copy(id = Some(pid))
      when(canterProductService.get()).thenReturn(Future.failed(new RuntimeException()))
      val result: Future[Result] = controller.getAll.apply(FakeRequest(GET, "/products"))
      status(result) mustBe INTERNAL_SERVER_ERROR
      contentAsString(result) mustBe "Products could not be retrieved"
    }
    "should get product with id" in {
      val testProduct = TestModels.canterProduct.copy(id = Some(pid))
      val products = Future(Some(testProduct))
      when(canterProductService.get(pid)).thenReturn(products)

      val result: Future[Result] = controller.get(pid).apply(FakeRequest(GET, "/products/" + pid.toString))
      val resultAsObj = Json.fromJson[CanterProduct](Json.parse(contentAsString(result))).get
      resultAsObj mustBe testProduct
      status(result) mustBe OK
      contentType(result) mustBe Some("application/json")
    }
    "should get product with id request fail and return internal server error if service throws exception" in {
      val testProduct = TestModels.canterProduct.copy(id = Some(pid))
      when(canterProductService.get(pid)).thenReturn(Future.failed(new RuntimeException()))

      val result: Future[Result] = controller.get(pid).apply(FakeRequest(GET, "/products/" + pid.toString))
      status(result) mustBe INTERNAL_SERVER_ERROR
      contentAsString(result) mustBe "Product could not be retrieved"
    }
    "should delete product with id" in {
      when(canterProductService.delete(pid)).thenReturn(Future(1))

      val result: Future[Result] = controller.delete(pid).apply(FakeRequest(DELETE, "/delete/" + pid.toString))
      contentAsString(result) mustBe "Product is deleted"
      status(result) mustBe OK
    }
    "should delete product with id request fail and return internal server error if service throws exception" in {
      when(canterProductService.delete(pid)).thenReturn(Future.failed(new RuntimeException()))

      val result: Future[Result] = controller.delete(pid).apply(FakeRequest(DELETE, "/delete/" + pid.toString))
      status(result) mustBe INTERNAL_SERVER_ERROR
      contentAsString(result) mustBe "Product could not be deleted"
    }
    "should create product" in {
      val testProduct = TestModels.canterProduct
      when(canterProductService.create(testProduct)).thenReturn(Future(pid))
      val json = Json.toJson(testProduct)
      val result: Future[Result] = controller.create().apply(FakeRequest(POST, "/products").withJsonBody(json).withHeaders(postHeaders))
      contentAsString(result) mustBe "Product is created"
      status(result) mustBe OK
    }
    "should not create product if request json is invalid " in {
      val testProductDetail = TestModels.canterProductDetail
      val json = Json.toJson(testProductDetail)
      val result: Future[Result] = controller.create().apply(FakeRequest(POST, "/products").withJsonBody(json).withHeaders(postHeaders))
      status(result) mustBe BAD_REQUEST
    }
    "should create product request fail and return internal server error if services throws exception" in {
      val testProduct = TestModels.canterProduct
      when(canterProductService.create(testProduct)).thenReturn(Future.failed(new RuntimeException()))
      val json = Json.toJson(testProduct)
      val result: Future[Result] = controller.create().apply(FakeRequest(POST, "/products").withJsonBody(json).withHeaders(postHeaders))
      status(result) mustBe INTERNAL_SERVER_ERROR
      contentAsString(result) mustBe "Product could not be created"
    }
    "should update product with id" in {
      val testProduct = TestModels.canterProduct
      when(canterProductService.update(testProduct)).thenReturn(Future(1))
      val json = Json.toJson(testProduct)
      val result: Future[Result] = controller.update(pid).apply(FakeRequest(POST, "/products/"+ pid.toString).withJsonBody(json).withHeaders(postHeaders))
      contentAsString(result) mustBe "Product is updated"
      status(result) mustBe OK
    }
    "should not update product if request json is invalid " in {
      val testProductDetail = TestModels.canterProductDetail
      val json = Json.toJson(testProductDetail)
      val result: Future[Result] = controller.update(pid).apply(FakeRequest(POST, "/products/"+ pid.toString).withJsonBody(json).withHeaders(postHeaders))
      status(result) mustBe BAD_REQUEST
    }
    "should update request fail and return internal server error if services throws exception" in {
      val testProduct = TestModels.canterProduct
      when(canterProductService.update(testProduct)).thenReturn(Future.failed(new RuntimeException()))
      val json = Json.toJson(testProduct)
      val result: Future[Result] = controller.update(pid).apply(FakeRequest(POST, "/products/"+ pid.toString).withJsonBody(json).withHeaders(postHeaders))
      status(result) mustBe INTERNAL_SERVER_ERROR
      contentAsString(result) mustBe "Product could not be updated"
    }
  }
}
