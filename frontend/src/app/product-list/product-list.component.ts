import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { IProductType } from '../models/product';
import { ProductService } from '../services/product.service';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})

export class ProductListComponent {
  dataSource: IProductType[]

  constructor(private router: Router, private productService: ProductService) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.productService.getAllProducts().subscribe(res => { this.dataSource = res });
  }

  onEdit(pid) {
    this.router.navigate(['add'], { queryParams: { id: pid } })
  }

  onDelete(id) {
    this.productService.onDelete(id).subscribe(
      () => {this.getData()}, 
    )
  }
  
}
