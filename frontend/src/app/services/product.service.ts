import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IProductType } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private readonly BASE_URL = 'http://localhost:9000/products';
  constructor(private http: HttpClient) { }

  public getAllProducts(): Observable<IProductType[]> {
    const headers = new HttpHeaders({ 'Access-Control-Allow-Origin': '*' });
    const response = this.http.get<IProductType[]>(`${this.BASE_URL}`, {
      headers: headers
    })
      .pipe(
        catchError
          (this.handleError<[]>('getAllProducts', []))
      )
    return response;
  }

  public get(id): Observable<IProductType> {
    const headers = new HttpHeaders({ 'Access-Control-Allow-Origin': '*' });
    return this.http.get<IProductType>(`${this.BASE_URL}/${id}`, {
      headers: headers
    }).pipe(
      catchError(this.handleError<IProductType>(`get id=${id}`))
    );
  }

  public onDelete(id) {
    console.log('deleting', id)
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      "Access-Control-Allow-Methods": "DELETE, POST, GET, OPTIONS"
    });

    return this.http.delete(`${this.BASE_URL}/${id}`, {
      headers: headers,
      responseType: 'text'
    })
    .pipe(
      catchError
        (this.handleError<[]>('onDelete', []))
    )
  }

  public onCreate(product) {
    console.log('creating')
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    });

    return this.http.post(`${this.BASE_URL}`, product, {
      headers: headers,
      responseType: 'text'
    })
    .pipe(
      catchError
        (this.handleError<[]>('onCreate', []))
    )
  }

  public onUpdate(product) {
    console.log('updating: ', product['id'] )
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    });

    return this.http.put(`${this.BASE_URL}/${product['id']}`, product, {
      headers: headers,
      responseType: 'text'
    })
    .pipe(
      catchError
        (this.handleError<[]>('onUpdate', []))
    )
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error('Error is=', error); // log to console instead
      return throwError(error);
    };
  }
}
