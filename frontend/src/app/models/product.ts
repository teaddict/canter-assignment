interface IProductDetailsType {
    key: string,
    value: string,
}

export interface IProductType {
    id: number,
    name: string,
    category: string,
    code: string,
    price: number,
    details: IProductDetailsType[]
}