import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../services/product.service';


@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {

  disabled: boolean = false;
  keyValue: Array<Object> = [];
  productForm: FormGroup;
  formData = {};

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute,
    private router: Router,
    private productService: ProductService) {

    this.productForm = this.formBuilder.group({
      name: ['', Validators.required],
      category: ['', Validators.required],
      code: ['', Validators.required],
      price: ['', Validators.required,],
      key: ['', Validators.required],
      value: ['', Validators.required],
      details: this.formBuilder.array([])
    });
  }

  onClick(): void {
    (this.productForm.get('details') as FormArray).push(
        this.formBuilder.group({
          key: [''],
          value: ['']
        })
    );
  }

  onChange() {
    this.disabled = false;
  }

  onSubmit(data): void {
    const productDetail = {
      name: data.value.name,
      category: data.value.category,
      code: data.value.code,
      price: data.value.price,
      details: data.value.details,
      id: this.formData['id']
    }
    if(this.formData['id'] == undefined) {
      this.productService.onCreate(productDetail).subscribe(response => {
        console.log(response);
        this.router.navigate(['']);
      }, error => {
        console.log(error);
      })
    }
    else {
      this.productService.onUpdate(productDetail).subscribe(response => {
        console.log(response);
        this.router.navigate(['']);
      }, error => {
        console.log(error);
      })
    }
  }

  ngOnInit(): void {
    if (this.route.snapshot.queryParamMap.get('id') != null) {
      this.productService.get(this.route.snapshot.queryParamMap.get('id')).subscribe(res => { 
        this.formData = res 
        if (this.formData != null) {
            this.productForm.patchValue({
            code: this.formData['code'],
            name: this.formData['name'],
            category: this.formData['category'],
            price: this.formData['price']
          });
          let productFormDetails = this.productForm.get('details') as FormArray;
          this.formData['details'].forEach( detail => {
            productFormDetails.push(this.formBuilder.group({
              key: detail['key'],
              value: detail['value']
            }));
        });
        }
      });
    }
  }
}

