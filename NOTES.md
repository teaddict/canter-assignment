# Notes About Assignment

## Backend 

### Controller

**CanterProductController** handles all requests related to **products**. It could be better to define a generic error type,
like all errors will be in json format and with some error code. Currently **InternalServerError** returns just a text. It needs to be decided
how much info we want to pass to client first. Then we can write the error handler, catch exceptions return with specific error code.

### Service

**CanterProductService** represents the business logic. It doesn't know about database or database operations. So later if we have to change our database etc, we dont need to rewrite everything. 

### Repository

**CanterProductDetailRepo** and **CanterProductRepo** handle all database operations. It uses **Slick 3** and **Mysql**. 

### Models

**CanterProductDetailDto** has **productId** which points to the **CanterProduct**. This way we can store as much as product detail.
And with the **ON DELETE CASCADE** , it will automatically remove all product details whenever we remove the pointed product.

### Testing

Unit tests are added. For the repository tests, I have used H2 in-memory database.
Integration test is missing.

## Frontend 
I didn't have much time to go through everything. Error handling is not visible to user. Needs to be decided some kind of error toast or error messages under form.
**ProductService** is responsible for all requests.
**onUpdate** navigates to add page with an id so we can fetch the latest version of object before editing it.
Product details needs a delete button in each row, so we can remove it.

### Database Setup
```
sudo docker pull mysql/mysql-server:5.7 
```
  
```
sudo docker run --name=mysqlCon -p 3306:3306 -d mysql/mysql-server:5.7  
```

```
sudo docker exec -it mysqlCon mysql -uroot -p  
```

```
ALTER USER 'root'@'localhost' IDENTIFIED BY 'test';  
```

```
GRANT ALL PRIVILEGES ON *.* to root@'%' IDENTIFIED BY 'root';  
```
### Database
```
CREATE DATABASE myProductDB;

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id`   BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `category` VARCHAR(255) NOT NULL,
  `code` VARCHAR(255) NOT NULL,
  `price` DECIMAL NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `productDetails` (
  `id`   BIGINT(20) NOT NULL AUTO_INCREMENT,
  `productId` BIGINT(20) NOT NULL,
  `key` VARCHAR(255) NOT NULL,
  `value` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
    FOREIGN KEY (`productId`)
        REFERENCES products (`id`)
        ON DELETE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
```